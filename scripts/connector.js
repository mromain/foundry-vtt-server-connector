const express = require('express');
const cors = require('cors');

// Register socket communication handlers
Hooks.once("socketlib.ready", () => {
  console.log("SERVER CONNECTOR - SocketLib is ready");
  const socket = socketlib.registerModule("server-connector");
  console.log("SERVER CONNECTOR - Socket registered for server-connector");

  // Register a handler for the handleRequest event
  socket.register("handleRequest", async (sessionId, entityType, entityName, ...args) => {
    console.log("SERVER CONNECTOR - Received request with sessionId:", sessionId, "entityType:", entityType, "entityName:", entityName, "args:", args);

    let collection;
    switch (entityType) {
      case 'actor':
        collection = game.actors;
        break;
      case 'item':
        collection = game.items;
        break;
      case 'scene':
        collection = game.scenes;
        break;
      case 'journal':
        collection = game.journal;
        break;
      case 'table':
        collection = game.tables;
        break;
      case 'playlist':
        collection = game.playlists;
        break;
      case 'card':
        collection = game.cards;
        break;
      default:
        throw new Error(`Unknown entity type '${entityType}'`);
    }

    const document = collection.find(doc => doc.name === entityName);

    if (!document) {
      throw new Error(`SERVER CONNECTOR - Document with name '${entityName}' not found in ${entityType}`);
    }

    // Return the document data
    return document.data;
  });

  console.log("SERVER CONNECTOR - Handler registered for handleRequest");

  // Register a handler for the handleRequest event
  socket.register("testApi", async () => {
    console.log("Received request");

    // Return the document data
    return game.actors;
  });

  console.log("SERVER CONNECTOR - Handler registered for testApi");
});

// Set up Express server
const app = express();
const port = 3030; // You can change the port if needed

// Use cors middleware
app.use(cors());

app.use(express.json());

app.post('/api/call-socketlib', async (req, res) => {
    const { sessionId, entityType, entityName, args } = req.body;
    if (!sessionId || !entityType || !entityName) {
        return res.status(400).send("Missing required parameters");
    }

    try {
        const result = await socketlib.getSocket("server-connector").executeAsGM("handleRequest", sessionId, entityType, entityName, ...args);
        res.json({ success: true, result });
    } catch (error) {
        res.status(500).json({ success: false, error: error.message });
    }
});

app.listen(port, () => {
    console.log(`SERVER CONNECTOR - Express server listening on port ${port}`);
});
